return {
	"rafi/awesome-vim-colorschemes",
    config = function()
        vim.cmd([[colorscheme minimalist]])
    end
}
